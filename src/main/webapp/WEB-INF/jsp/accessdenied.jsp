<%@ page import ="org.springframework.security.core.*,org.springframework.security.core.context.*" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
    <h1>Access Denied Error Page</h1>
    <br><a href="/logout">logout</a>
    <br><br>
    <%
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    if (auth != null) {
    out.println("User '" + auth.getName() + "' attempted to access the protected URL: ");
    out.println("<br>auth : "+auth.isAuthenticated());
    out.println("<br>Role : "+auth.getAuthorities());
    out.println("<br>Error Page : "+request.getRequestURL());
    }
    %>
</body>
</html>