<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
  <head>
   <meta charset="utf-8">
       <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
       <meta name="description" content="">
       <meta name="author" content="">
       <title>Login Page</title>
       <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
       <link href="https://getbootstrap.com/docs/4.0/examples/signin/signin.css" rel="stylesheet" crossorigin="anonymous"/>
    </head>
  <body>
   <div class="container">
         <form class="form-signin" method="post" action="/login">
           <h2 class="form-signin-heading">Login Page</h2>
           <c:if test="${param.error}">
               <span style="color:red">Invalid username or password.</span>
           </c:if>
           <p>
             <label for="username" class="sr-only">Username</label>
             <input type="text" id="username" name="username" class="form-control" placeholder="Username" required autofocus>
           </p>
           <p>
             <label for="password" class="sr-only">Password</label>
             <input type="password" id="password" name="password" class="form-control" placeholder="Password" required>
           </p>
   <input name="_csrf" type="hidden" value="a6c62694-4ea5-4882-b84a-946ef44d1313" />
           <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
         </form>
   </div>
</body></html>