package com.coditas.springbootdemo.user.repository;

import com.coditas.springbootdemo.user.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,Integer> {
    public User findByEmail(String email);
}
