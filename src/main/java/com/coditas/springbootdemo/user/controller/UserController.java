package com.coditas.springbootdemo.user.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/user")
public class UserController {

    @RequestMapping("/dashboard")
    public ModelAndView userDashboard() {
        return new ModelAndView("user-dashboard");
    }
}
