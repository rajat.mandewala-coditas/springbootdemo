package com.coditas.springbootdemo.login;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LoginController {

    @GetMapping(value={"/","/login"})
    public ModelAndView getLoginPage() {
        return new ModelAndView("login");
    }

    @RequestMapping("/accessdenied")
    public ModelAndView getAccessDeniedPage() {
        return new ModelAndView("accessdenied");
    }
}