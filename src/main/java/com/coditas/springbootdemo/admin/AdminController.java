package com.coditas.springbootdemo.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/admin")
public class AdminController {

    @RequestMapping("/dashboard")
    public ModelAndView GetAdminDashboard() {
        return new ModelAndView("admin-dashboard");
    }
}
